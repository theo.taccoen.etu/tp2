export class Component {
	children;
	attribute;
	tagName;

	constructor(tagName, attribute, children) {
		this.children = children;
		this.attribute = attribute;
		this.tagName = tagName;
	}

	render() {
		let str = '';
		if (this.children instanceof Array) {
			this.children.forEach(element => {
				str += element;
			});
		} else if (this.children instanceof Component) {
		} else {
			str = this.children;
		}

		if (this.attribute === null && this.children === null) {
			return `<${this.tagName} />`;
		} else if (this.children === undefined && this.attribute !== null) {
			return `<${this.tagName} ${this.attribute.name}=\"${this.attribute.value}\" />`;
		} else if (this.attribute === null) {
			return `<${this.tagName}>${str}</${this.tagName}>`;
		} else {
			return `<${this.tagName} ${this.attribute.name}=\"${this.attribute.value}\">${str}</${this.tagName}>`;
		}
	}
}

export class Img extends Component {}

// const title = new Component('h1', null, ['La', ' ', 'carte']);
// document.querySelector('.pageTitle').innerHTML = title.render();

// const img = new Img('img', {
// 	name: 'src',
// 	value:
// 		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300',
// });

//document.querySelector('.pageContent').innerHTML = img.render();

const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);
document.querySelector('.pageContent').innerHTML = c.render();

// retourne : <article class="pizzaThumbnail">Regina</article>

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image:
			'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300',
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image:
			'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300',
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image:
			'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	},
];

export default data;
